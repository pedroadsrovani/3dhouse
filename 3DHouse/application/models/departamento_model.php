<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento_Model extends CI_Model {
	
	public function getTotal($condicao = array()) {#fução get total recupera todos os itens e conta de departamento para paginação
		$this->db->where($condicao);
		$this->db->from('departamento');
		return $this->db->count_all_results();
	}
	
	
	public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_PAINELADMIN){#se $limite for false a pesquisa não irá se lmitar a 7 pesquisas
		$this->db->select('d.codepartamento, d.nomedepartamento, d.coddepartamentopai');
		$this->db->select('p.nomedepartamento as nomepai');
		$this->db->where($condicao);
		$this->db->from('departamento d');
		$this->db->join('departamento p', 'p.codepartamento = d.coddepartamentopai', 'LEFT');
		# left join entre codepartamento e coddpertamentopai 

		if ($primeiraLinha) {
			return $this->db->get()->first_row();
		} else {
			 if($limite !== FALSE){ 
                  $this->db->limit($limite, $pagina);#limita o numero de tipos de atributos pela quantidade de itens exibidos na pagina. 

             } 
      
			return $this->db->get()->result();
		   }
     }
	/* função post para inserção */
	public function post($itens) {
		$res = $this->db->insert('departamento', $itens);
		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}
	/* função update */
	public function update($itens, $codepartamento) {
		$this->db->where('codepartamento', $codepartamento, FALSE);
		$res = $this->db->update('departamento', $itens);
		if ($res) {
			return $codepartamento;
		} else {
			return FALSE;
		}
	}
	/* função delete */
	public function delete($codepartamento) {
		$this->db->where('codepartamento', $codepartamento, FALSE);
		return $this->db->delete('departamento');
	}
}