<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto_Model extends CI_Model{  


    #função de paginação do itens da tabela 
public function getTotal($condicao = array()){ 
    $this->db->where($condicao); 
    $this->db->from('produto'); 
    return $this->db->count_all_results();#contador de todos os itens da pesquisa 

}


public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_PAINELADMIN ){ 

  $this->db->select('codproduto, nomeproduto, resumoproduto, fichaproduto, valorproduto, valorpromocional, codtipoatributo, urlseo'); 
  $this->db->where($condicao); 
  $this->db->from('produto'); 

  if ($primeiraLinha){ 
    return  $this->db->get()->first_row(); 

  }else{  
      if($limite !== FALSE){ 
          $this->db->limit($limite, $pagina);#limita o numero de tipos de atributos pela quantidade de itens exibidos na pagina. 

      } 
      
      return $this->db->get()->result(); 
    
    }
} 


   public function post($itens){ 
     $res =  $this->db->insert('produto', $itens); 
        if($res){ 
            return $this->db->insert_id();
        }else{ 

            return FALSE;
        }
   } 


#update produto
public function update($itens, $codproduto) {
		$this->db->where('codproduto', $codproduto, FALSE);
		$res = $this->db->update('produto', $itens);
		if ($res) {
			return $codproduto;
		} else {
			return FALSE;
		}
	}


   #metodo excluir admins 
   public function delete($codproduto){  
       $this->db->where('codproduto', $codproduto, FALSE);
       return $this->db->delete('produto');

   }
}