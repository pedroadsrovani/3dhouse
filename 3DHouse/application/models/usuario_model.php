<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_Model extends CI_Model{  //classe admin_model para admin


    #função de paginação do itens da tabela 
public function getTotal($condicao = array()){ #
    $this->db->where($condicao); 
    $this->db->from('usuario'); 
    return $this->db->count_all_results();#contador de todos os itens da pesquisa 

}

/* select e retorno da tabela admin  */
public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0){ 

  $this->db->select('codusuario, nome, email, cpf, cep, rua, bairro, complemento, cidade, estado, telefone, sexo, perfil'); 
  $this->db->where($condicao); 
  $this->db->from('usuario'); 

  if ($primeiraLinha){ #se retornar uma linha = True
    return  $this->db->get()->first_row(); #utilizado para editar o objeto na função editar do controler

  }else{  #A VARIÁVEL LINHAS_PESQUISA_PAINELADMIN é uma variável constante definida com valor 7 
      $this->db->limit(LINHAS_PESQUISA_PAINELADMIN, $pagina);#senão retorna todos os itens com um limite especificado
      return $this->db->get()->result(); #retorna os resultados 
    
    }
}

 /* função post para insert  */
   public function post($itens){ //itens e o parametro do objeto criado em admin no controler
     $res =  $this->db->insert('usuario', $itens); 
        if($res){ //se adicionou corretamente
            return $this->db->insert_id();//retorna o ultimo código do item
        }else{ //retorna false

            return FALSE;
        }
   } 


 #update admin 
   public function update($itens, $codusuario) {#metodo de edição recebe como parametros o objeto, e o codigo do admin
		$this->db->where('codusuario', $codusuario, FALSE);#condição do update como condição codigo do admin paramentro false para numeros inteiros
		$res = $this->db->update('usuario', $itens);
		if ($res) {#retrona  o codigo do admin
			return $codusuario;
		} else {
			return FALSE;
		}
	}


   #metodo excluir admins 
   public function delete($codusuario){ #recebe como parametro o codigo do admin 
       $this->db->where('codusuario', $codusuario, FALSE);#condição para exclusão codadmin equivale a $codadmin passado no parametro e parametro false para valores inteiros
       return $this->db->delete('usuario');#return da condição

   }
} 
#definir  a variavel LINHAS_PAINELADMIN uma constante no arquivo constants.php e atribuir o valor 7