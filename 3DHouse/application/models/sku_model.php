<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sku_Model extends CI_Model {
	
	/**
	 * Retorna SKU para produtos simples
	

	 */
	public function getPorProdutoSimples($codproduto) {
		$this->db->select('s.codsku, s.referencia, s.quantidade, s.codproduto');
		$this->db->select("'Produto Simples' AS nomeatributo", FALSE);
		$this->db->from('sku s');
		$this->db->where('s.codproduto', $codproduto, FALSE); 

		return $this->db->get()->result();
	}
	
	/**
	 * Retorna SKUs de produtos com atributos
	
	 */
	public function getPorProdutoAtributo($codproduto) {
		$this->db->select('s.codsku, s.referencia, s.quantidade, s.codproduto');
		$this->db->select('a.nomeatributo');
		$this->db->from('sku s');
		$this->db->from('skuatributo sa');
		$this->db->from('atributo a');
		$this->db->where('sa.codsku', 's.codsku', FALSE);
		$this->db->where('a.codatributo', 'sa.codatributo', FALSE);
	    $this->db->where('s.codproduto', $codproduto, FALSE);
		return $this->db->get()->result();
	}
	
	//lista os atributos disponíveis vinculados com skus por meio da tabela skuatributo usando como parametro o codigo do produto
	public function getAtributosDisponiveis($codproduto) {
		$this->db->select('a.nomeatributo, a.codatributo');
		$this->db->from('atributo a');
		$this->db->where("a.codatributo NOT IN (SELECT sa.codatributo FROM skuatributo sa, sku s WHERE sa.codsku = s.codsku AND s.codproduto = {$codproduto})");

		return $this->db->get()->result();
	}
	//armazena na tabela sku
	public function post($itens){
		$res = $this->db->insert('sku', $itens);
		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	} 
	//atualiza skus campos referencia e quantidade
	
	public function update($codsku, $itens){
		$this->db->where('codsku', $codsku, FALSE);
		$res = $this->db->update('sku', $itens);
	}
	//deleta skus
	public function delete($codsku) {
		$this->db->where('codsku', $codsku, FALSE);
		$res = $this->db->delete('sku');
	}
	
	//armazena na tabela skuatributo
	public function postAtributo($itens){
		$res = $this->db->insert('skuatributo', $itens);
		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}
}