<legend> 
Manutenção de Usuários - Editar<!--novo saber que esta adicionando novo admin -->

</legend> 

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open(uri_string());?>

     <label>Nome<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  type="text" id="first_name" class="form-control" name="first_name" value="" placeholder="Digite um nome...">
     <label class="control-label" for="nome"></label>
  </div>

</br> 

<label>Sobrenome<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  type="text" id="last_name" class="form-control" name="last_name" value="" placeholder="Digite um sobrenome...">
     <label class="control-label" for="nome"></label>
  </div>

</br>
      

<label>Digite um cpf para o usuário<font color="#FF0000"> *</font></label>
   <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  type="text" id="cpf" class="form-control" name="cpf" value="">
     <label class="control-label" for="cpf"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Sem traços. Ex:12345678911
    </small>  
  </br> 
</br>

     <!-- <p>
            <?php// echo lang('create_user_company_label', 'company');?> <br />
            <?php// echo form_input($company);?>
      </p>-->
<label>E-mail <font color="#FF0000"> *</font></label>
 <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
    <input  type="email" id="email" class="form-control" name="email" value="" placeholder="Digite o e-mail...">
     <label class="control-label" for="email"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Exemplo: example@gmail.com
    </small> 
      <!--<p>
            <?php //echo lang('create_user_email_label', 'email');?> <br />
            <?php //echo form_input($email);?>
      </p>--> 
<br/> 
<br>
      <label>Telefone <font color="#FF0000"> *</font></label>
 <div class="input-group">
    <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
    <input  type="text" id="phone" class="form-control" name="phone" value="" placeholder="Digite o e-mail...">
     <label class="control-label" for="email"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Exemplo:(DDD) xxxx xx xx
    </small>  
<br> 
<br>
    <label>Sexo:</label> 
</br>
<label class="radio-inline"><input type="radio" name="sexo"  id="sexo" value="masculino" {chk_sexo}><b>Masculino</b></label>
<label class="radio-inline"><input type="radio" name="sexo" id="sexo" value="feminino" {chk_sexo}><b>Feminino</b></label>
  
    

      <!--<p>
            <?php //echo lang('create_user_phone_label', 'phone');?> <br />
            <?php //echo form_input($phone);?>
      </p>-->
</br>
</br>
<label>Senha <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input  type="password" id="password" class="form-control" name="password" >
     <label class="control-label" for="password"></label> 
     
  </div> 
  <small id="passwordHelpInline" class="text-muted">
      A senha deve conter de 8 a 20 caracteres.
    </small>  
    </br>
    </br>
      <!--<p>
            <?php //echo lang('create_user_password_label', 'password');?> <br />
            <?php //echo form_input($password);?>
      </p>-->
<label>Confirme a senha <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input  type="password" id="password_confirm" class="form-control" name="password_confirm" >
     <label class="control-label" for="password_confirm"></label> 
     
  </div>  
  <br/> 
  <br/>
      <!--<p>
            <?php //echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php //echo form_input($password_confirm);?>
      </p>--> 
      <label>Digite um cep e depois clique em Consultar <font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="cep" class="form-control" style="width:50%" name="cep" value="{cep}">
     <label class="control-label" for="cep"></label>  <button id="btn_consulta" style="margin-left: 10px;" class="btn btn-primary" type="button" >Consultar</button>
  </div>  

</br> 
<label>Cidade<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="cidade" class="form-control" style="width:50%" name="cidade" value="{cidade}"/>
     <label class="control-label" for="cidade"></label> 

  </div>   
   
  </br>
<label>Estado<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="estado" class="form-control" style="width:50%" name="estado" value="{estado}"  />
     <label class="control-label" for="estado"></label>
  </div>  
  <br>
<label>Rua<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="rua" class="form-control" name="rua" value="{rua}">
     <label class="control-label" for="rua"></label>
  </div>  
   
  </br>
<label>Bairro<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="bairro" class="form-control" name="bairro" value="{bairro}">
     <label class="control-label" for="bairro"></label>
  </div>  
 
   
  </br> 

  <div class="form-group">
  <label class="control-label" for="complemento">Complemento (opcional)</label> 
    
       <textarea class="form-control" id="complemento" name="complemento" rows="6" placeholder="Digite aqui...">{complemento}</textarea>

     <label class="control-label" for="complemento"></label>
  </div>   


     

      <?php if ($this->ion_auth->is_admin()): ?>

         <label class="control-label" for="complemento">Grupo de usuário</label> 
          <?php foreach ($groups as $group):?>
              <label class="checkbox">
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
              <input type="radio" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
              </label> 

          <?php endforeach?> 
          <small id="passwordHelpInline" class="text-muted">
      Cada grupo de usuário possui permissões e privilégios diferentes.
    </small>  

      <?php endif ?>

      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?> 
      <br>
</br>
      <button type="submit" class="btn btn-primary" name="login_submit_btn" >Salvar</button><button type="button" style="margin-left:10px;" class="btn btn-danger" name="voltar" >Voltar</button>

<?php echo form_close();?>
