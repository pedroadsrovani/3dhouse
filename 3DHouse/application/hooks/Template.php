<?php 
// O controle da template do painel está na pasta hooks pois o controle tem que sobrescrever as funções de display do codeigniter  -->

if (!defined('BASEPATH')) exit('No direct script access allowed'); 
class Template{ //classe template Template.php é um arquivo de gerenciamento de layout.

    public function init(){ #função init
        $CI = &get_instance(); //instancia que esta rodando do codeigniter.

        $output = $CI->output->get_output();// captura o que esta sendo exibido pelo codeigniter. 

        if (isset($CI->layout)){ //condição que define qual layout sera exibido.

            if ($CI->layout){  #define se haverá um layout
                if($CI->layout == LAYOUT_PAINELADMIN){ #verificarse o layout é o do painel admin 
                # mensagens de controle de operações realizadas no paineladmin
                    $erroDash = $CI->session->flashdata('erro');  #acusa um erro
                    $sucessoDash = $CI->session->flashdata('sucesso'); #carrega o layout
                       
                       
                }

                if(!preg_match('/(.+).php$/', $CI->layout)){//verifica se o arquivo de layout possui extensão php. 

                   $CI->layout .='.php';//define a extensão do arquivo em .php manualmente.
                } 
                $template = APPPATH . 'templates/'.$CI->layout; //define o local da template na pasta da aplicação.

                        if (file_exists($template)){ //se o arquivo existir.
                            $layout = $CI->load->file($template, TRUE);// carrega o seu conteudo. 

                    }else{  
                        die('Template inválida.');//senão existir mensagem template invalida.e volta a aplicação.


                    }    
                    //this.parser.parse define variaveis no layout
                    //controle de mensagens de erro no bloco conteudo{BLC_CONTEUDO} 
                    //{conteudo} será substituido pelo output ou seja aquilo que estava senbdo exibido pelo codeigniter

                      $html = str_replace("{CONTEUDO}", $output, $layout);//define o conteudo da template. 

                      if($erroDash){ #se a variavel erro estiver setada  
                          #é carregado a função cria alerta para erro
                          $erroDash = $this->criaAlerta($erroDash, 'alert-error', 'erro'); #a variavel $erroDash recebe um alerta
                         # str_replace replica para outras paginas como editar, excluir, listar etc...
                         $html = str_replace("{MENSAGEM_SISTEMA_ERRO}", $erroDash, $html); #a variavel html recebe a mensagem de erro do erroDash 
                      }   
                      else{  
                          $html = str_replace("{MENSAGEM_SISTEMA_ERRO}", null, $html); 

                      }

                       if($sucessoDash){ #se sucesso estiver setada   
                        #é carregado a função cria alerta para sucesso
                           $sucessoDash = $this->criaAlerta($sucessoDash, 'alert-success', 'sucesso'); # sucessodash receber o alerta
                         $html = str_replace("{MENSAGEM_SISTEMA_SUCESSO}", $sucessoDash, $html);# html recebe mensagem do sistema mais uma replicação para as outras paginas
                      }else{ 
                           $html = str_replace("{MENSAGEM_SISTEMA_SUCESSO}", null, $html); # se não for setada define a mensagem como null
                      }
            }else{ 

                $html = $output;//se não foi definido o layout como false
            }
        }else{ 

                $html = $output; 
                
            } 

            $CI->output->_display($html);
    } 
//alerta personalizado do bootstrap de sucesso e erro
    private function criaAlerta($mensagem, $tipo, $titulo) {# recebe como poarametro a mensagem o tipo e o titulo da mensagem
		$html	= "<div class=\"alert {$tipo}\">\n";
		$html	.="\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n";
		$html	.="\t<strong>{$titulo}!</strong> {$mensagem}\n";
		$html	.="</div>";
		return $html;
	}
} 
# passo 5 criar uma pasta dentro da pasta controler e depois um arquivo .php(principal.php)onde irá conter a classe principal