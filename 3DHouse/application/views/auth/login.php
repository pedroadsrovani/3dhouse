 <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet"> 
 
<div class="container">    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title"><?php echo lang('login_heading');?></div>
                       
                    </div>     
<?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?= $this->session->flashdata('message') ?>
                        </div>
                    <?php endif; ?>

<div style="padding-top:30px" class="panel-body" >

                        
                            
                       

<?=form_open('auth/login')?>

<label for="identity">E-mail:</label>
<div style="margin-bottom: 2px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="identity" type="text" class="form-control" name="identity" value="" placeholder="Digite seu e-mail...">                                        
                                    </div>
  
  <label for="identity">Senha:</label>
<div style="margin-bottom: 2px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="password" type="password" class="form-control" name="password" value="" >                                        
                                    </div> 
									<small id="passwordHelpInline" class="text-muted">
      A senha deve conter no mínimo 8 caracteres.
    </small> 

  </br> 
  </br>

  <p>
    
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> 
	<?php echo lang('login_remember_label', 'remember');?>
  </p>


 
<button type="submit" class="btn btn-primary" name="login_submit_btn" >Login</button><button type="button" style="margin-left:10px;" class="btn btn-danger" name="voltar" >Voltar</button>
<?php echo form_close();?> 

<hr width="500">

<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p> 
</div> 
<a href="<?php echo site_url('recuperar_senha/forgot_password');?>" title="Listar usuarios" class="btn pull-right"><button type="button" class="btn btn-primary"><em class="glyphicon glyphicon-th-list"></em> Listar</button></a>  
