<?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?= $this->session->flashdata('message') ?>
                        </div>
                    <?php endif; ?>
<legend> 
Manutenção de Usuários - Novo<!--novo saber que esta adicionando novo admin -->

</legend>  



<?php echo form_open("auth/create_user");?> 

<label>Nome<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  type="text" id="first_name" class="form-control" name="first_name" value="" placeholder="Digite um nome...">
     <label class="control-label" for="nome"></label>
  </div>

</br> 

<label>Sobrenome<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  type="text" id="last_name" class="form-control" name="last_name" value="" placeholder="Digite um sobrenome...">
     <label class="control-label" for="nome"></label>
  </div>

</br>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

<label>Digite um cpf para o usuário<font color="#FF0000"> *</font></label>
   <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  type="text" id="cpf" class="form-control" name="cpf" value="">
     <label class="control-label" for="cpf"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Sem traços. Ex:12345678911
    </small>  
  </br> 
</br>

      
<label>E-mail <font color="#FF0000"> *</font></label>
 <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
    <input  type="email" id="email" class="form-control" name="email" value="" placeholder="Digite o e-mail...">
     <label class="control-label" for="email"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Exemplo: example@gmail.com
    </small> 
      <!--<p>
            <?php //echo lang('create_user_email_label', 'email');?> <br />
            <?php //echo form_input($email);?>
      </p>--> 
<br/> 
<br>
      <label>Telefone <font color="#FF0000"> *</font></label>
 <div class="input-group">
    <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
    <input  type="text" id="phone" class="form-control" name="phone" value="" placeholder="Digite o e-mail...">
     <label class="control-label" for="email"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Exemplo:(DDD) xxxx xx xx
    </small>  
<br> 
<br>
    <label>Sexo:</label> 
</br>
<label class="radio-inline"><input type="radio" name="sexo"  id="sexo" value="masculino" {chk_sexo}>Masculino</label>
<label class="radio-inline"><input type="radio" name="sexo" id="sexo" value="feminino" {chk_sexo}>Feminino</label>
  
    

      <!--<p>
            <?php //echo lang('create_user_phone_label', 'phone');?> <br />
            <?php //echo form_input($phone);?>
      </p>-->
</br>
</br>
<label>Senha <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input  type="password" id="password" class="form-control" name="password" >
     <label class="control-label" for="password"></label> 
     
  </div> 
  <small id="passwordHelpInline" class="text-muted">
      A senha deve conter de 8 a 20 caracteres.
    </small>  
    </br>
    </br>
      <!--<p>
            <?php //echo lang('create_user_password_label', 'password');?> <br />
            <?php //echo form_input($password);?>
      </p>-->
<label>Confirme a senha <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input  type="password" id="password_confirm" class="form-control" name="password_confirm" >
     <label class="control-label" for="password_confirm"></label> 
     
  </div>  
  <br/> 
  <br/>
      <!--<p>
            <?php //echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php //echo form_input($password_confirm);?>
      </p>--> 
      <label>Digite um cep e depois clique em Consultar <font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="cep" class="form-control" style="width:50%" name="cep" value="">
     <label class="control-label" for="cep"></label>  <button id="btn_consulta" style="margin-left: 10px;" class="btn btn-primary" type="button" >Consultar</button>
  </div>  

</br> 
<label>Cidade<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="cidade" class="form-control" style="width:50%" name="cidade" value=""/>
     <label class="control-label" for="cidade"></label> 

  </div>   
   
  </br>
<label>Estado<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="estado" class="form-control" style="width:50%" name="estado" value=""  />
     <label class="control-label" for="estado"></label>
  </div>  
  <br>
<label>Rua<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="rua" class="form-control" name="rua" value="">
     <label class="control-label" for="rua"></label>
  </div>  
   
  </br>
<label>Bairro<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="bairro" class="form-control" name="bairro" value="">
     <label class="control-label" for="bairro"></label>
  </div>  
 
   
  </br> 

  <div class="form-group">
  <label class="control-label" for="complemento">Complemento (opcional)</label> 
    
       <textarea class="form-control" id="complemento" name="complemento" rows="6" placeholder="Digite aqui..."></textarea>

     <label class="control-label" for="complemento"></label>
  </div>   

<br>  
     
<button type="submit" class="btn btn-primary">Salvar</button>
        <?=anchor('auth/index','Cancelar',['class'=>'btn btn-danger'])?>

<?php echo form_close();?>
