   
<legend>
Manutenção de Produtos - {ACAO}
	
</legend>
<form action="{ACAOFORM}" method="post" class="form-horizontal"> 
   
	       <input type="hidden" name="codproduto" id="codproduto" value="{codproduto}">
	
	          <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-th-large"></i></span>
                <input type="text" id="nomeproduto" class="form-control" name="nomeproduto" value="{nomeproduto}" required="required" placeholder="Digite um nome de produto">
                <label class="control-label" for="nomeproduto"></label>
           </div>  
           <br> 

              <div class="form-group">
                 <label for="resumoproduto">Resumo do produto:</label>
                 <textarea class="form-control" id="resumoproduto" name="resumoproduto" rows="6" placeholder="Digite o resumo do produto aqui...">{resumoproduto}</textarea>
            </div>
            <br>
	                <div class="form-group">
                      <label for="fichaproduto">Ficha técnica do produto:</label>
                      <textarea class="form-control" id="fichaproduto" name="fichaproduto" rows="6" placeholder="Digite informações da ficha técnica do produto aqui...">{fichaproduto}</textarea>
                 </div>
                 <br> 

                       <label>Digite um valor pro produto:</label>
                       <div class="input-group"> 
                          <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                          <input type="text" id="valorproduto"  class="form-control set-numeric" name="valorproduto" value="{valorproduto}" required="required" placeholder="">
                          <label class="control-label" for="valorproduto"></label>
                      </div>  
                      <br>  

                           <label>Digite um valor promocional pro produto: (opcional)</label>
                           <div class="input-group"> 
                               <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                              <input type="text" id="valorpromocional" class="form-control set-numeric" name="valorpromocional" value="{valorpromocional}"><!-- classe set-numeric  será utilizado como referencia pra o plugin do jquery maskedinput percorrer e formatar utilizando uma mascara especifica para valor-->
                               <label class="control-label" for="valorpromocional"></label>
                           </div>   

                           <br>
                    
                             <!--form de tipo de atributo-->
	                                   <div class="control-group">
		                                    <label class="control-label" for="codtipoatributo">Tipo de atributo pertencente ao produto:</label>
	                                          <div class="controls">
	    	                                          <select class="form-control" name="codtipoatributo" id="codtipoatributo" {des_tipoatributo} class="set-quantidade-sku"><!-- {des_tipoatributo} =  desabilita o form tipoatributo no update -->
	    	                                          <option value="">Não especificado</option>
	    	                                        	{BLC_TIPOATRIBUTOS}
	    		                                       <option value="{CODTIPOATRIBUTO}" {sel_sel_codtipoatributo}>{NOME}</option>
	    		                                        {/BLC_TIPOATRIBUTOS}
	    	                                          </select>
	                                            </div>
	                                    </div> 
                                       
                            
  	                                  
		                                 
	       
             <br>   
             <br> 
             <!-- form de departamento-->
             <div class="row-fluid"> 
                    <div class="span8"> 
                    </div> 
            </div>  
                         <div class="span6">  
                         
                            <h4>Departamentos</h4>  
                                <ul class="list-unstyled">  
                                {BLC_DEPARTAMENTOPAI}   <!--bloco que busca o cod do departamento pai -->                                                                                        <!--são vários departamentos então tem que vim em um array -->                  <!-- lista o nome do departamento pai--> 
                                     <li><label for="departamento-{CODDEPARTAMENTO}" class="radio-inline checkbox"><input {chk_departamentopai} name="departamento[]" class="set-departamento-pai" type="checkbox" id="departamento-{CODDEPARTAMENTO}" value="{CODDEPARTAMENTO}">{NOMEDEPARTAMENTO}</label></li> 
                                     <li>                                                                                                                             <!--classe de referencia para codigo javascript loja.js -->                         
                                         <ul>
                                       {BLC_DEPARTAMENTOFILHO} <!--bloco que lista  o departamento filho (cod)-->  
                                                                                                                                                      --> 
                                             <li><label for="departamento-{CODDEPARTAMENTOFILHO}" class="radio-inline checkbox"><input {chk_departamentofilho} name="departamento[]" class="set-departamento-filho" data-pai="{CODDEPARTAMENTOPAI}" type="radio" id="departamento-{CODDEPARTAMENTOFILHO}" value="{CODDEPARTAMENTOFILHO}">{NOMEDEPARTAMENTOFILHO}</label></li>
                                                                                                                            pai -->
                                       {/BLC_DEPARTAMENTOFILHO}  
                                        </ul>
                                    </li>
                                       {/BLC_DEPARTAMENTOPAI}
                               </ul>
                          </div> 
                    <br>    


                    <br>   
                   <button type="submit" class="btn btn-primary">Salvar</button>  <a href="{URLLISTAR}" title="Listar produtos" class="btn btn-primary">Voltar</a><!--joga os usuarios listados a direita -->
</form>