
<div class="container"> 

	<div class="row">
      <div class="col-lg-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="row"> 
            <a href="<?php echo site_url('Gallery') ?>">
              <div class="col-xs-6">
                <i class="fa fa-file-image-o fa-5x" aria-hidden="true"></i>
              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading"><?php   $query	=	$this->db->count_all('imagens'); echo $query  ?></p>
                <p class="announcement-text">Banners</p>
              </div>
            </div>
          </div>
         </a>
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                    <a href="<?php echo site_url('Gallery') ?>">Listar Banners</a>
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel panel-warning">
          <div class="panel-heading">
            <div class="row"> 
                 <a href="<?php echo site_url('painel/produto') ?>">
              <div class="col-xs-6">
                <i class="fa fa-barcode fa-5x"></i>
              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading"><?php   $query	=	$this->db->count_all('produto'); echo $query  ?></p>
                <p class="announcement-text"> Produtos</p>
              </div>
            </div>
          </div>
          </a>
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  <a href="<?php echo site_url('painel/produto') ?>">Listar Produtos</a>
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel panel-danger">
          <div class="panel-heading">
            <div class="row"> 
                 <a href="<?php echo site_url('auth/index') ?>">
              <div class="col-xs-6">
                <i class="fa fa-users fa-5x"></i>
              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading"><?php   $query	=	$this->db->count_all('users'); echo $query  ?></p>
                <p class="announcement-text">Usuários</p>
              </div>
            </div>
          </div>
          </a>
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                  <a href="<?php echo site_url('painel/usuario') ?>">Listar Usuários</a>
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="row"> 
                 <a href="<?php echo site_url('painel/departamento') ?>">
              <div class="col-xs-6">
                <i class="fa fa-th-list fa-5x" aria-hidden="true"></i>

              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading"><?php   $query	=	$this->db->count_all('departamento'); echo $query  ?></p>
                <p class="announcement-text"> Depto</p>
              </div>
            </div>
          </div>
          </a>
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
             <a href="<?php echo site_url('painel/departamento') ?>">Listar Departamentos</a>
 
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div> 
             
             <div class="col-lg-4">
        <div class="panel panel-warning">
          <div class="panel-heading">
            <div class="row"> 
                 <a href="<?php echo site_url('painel/tipoatributo') ?>">
              <div class="col-xs-6">
                <i class="fa fa-bars fa-5x" aria-hidden="true"></i>

              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading"><?php   $query	=	$this->db->count_all('tipoatributo'); echo $query  ?></p>
                <p class="announcement-text">Tipos</p> 
                
              </div>
            </div>
          </div>
         </a>

            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
                         <a href="<?php echo site_url('painel/tipoatributo') ?>">Tipos de Atributos</a>
  
                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div> 
    </a>
        <div class="col-lg-4">
        <div class="panel panel-danger">
          <div class="panel-heading">
            <div class="row">  
                
                 <a href="<?php echo site_url('painel/atributo') ?>">
              <div class="col-xs-6">
                <i class="fa fa-list fa-5x" aria-hidden="true"></i>

              </div>
              <div class="col-xs-6 text-right">
                <p class="announcement-heading"><?php   $query	=	$this->db->count_all('atributo'); echo $query  ?></p>
                <p class="announcement-text"> Atributos</p>
              </div>
            </div>
          </div>
        </a>  
            <div class="panel-footer announcement-bottom">
              <div class="row">
                <div class="col-xs-6">
             <a href="<?php echo site_url('painel/atributo') ?>">Listar Atributos</a>

                </div>
                <div class="col-xs-6 text-right">
                  <i class="fa fa-arrow-circle-right"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>

    </div><!-- /.row -->
    </div>