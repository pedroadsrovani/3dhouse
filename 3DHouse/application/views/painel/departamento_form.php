<legend>
Manutenção de Departamentos - {ACAO}
	
</legend>
<form action="{ACAOFORM}" method="post" class="form-horizontal">
	<input type="hidden" name="codepartamento" id="codepartamento" value="{codepartamento}">
	
	<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
    <input type="text" id="nomedepartamento" class="form-control" name="nomedepartamento" value="{nomedepartamento}" required="required" placeholder="Digite um nome de departamento...">
     <label class="control-label" for="nomedepartamento"></label>
  </div>  
<br>
	
<!--  formulario do departamento pai-->
	<div class="control-group">
		<label class="control-label" for="coddepartamentopai">Departamento Pai:</label>
	    <div class="controls">
	    	<select class="form-control" name="coddepartamentopai" id="coddepartamentopai" {hab_coddepartamentopai}><!-- {hab_coddepartamentopai} não poderá atribuit um departamento pai a um outro departamento pai -->
	    		<option value="">Selecione um departamento</option>
	    		{BLC_DEPARTAMENTOS}
	    		<option value="{CODDEPARTAMENTO}" {sel_coddepartamentopai}>{NOME}</option><!-- {sel_coddepartamentopai} desabilita a troca de departamento pai do departamento que esta sendo editado -->
	    		{/BLC_DEPARTAMENTOS}
	    	</select>
	    </div>
	</div>
  	<br>
		<button type="submit" class="btn btn-primary">Salvar</button>  <a href="{URLLISTAR}" title="Listar Departamentos" class="btn btn-primary">Voltar</a><!--joga os usuarios listados a direita -->
	
</form>