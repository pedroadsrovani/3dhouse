
<legend>
	Vincular Fotos com Atributos do Produto: {NOMEPRODUTO}
	<div class="pull-right">
		
	</div>
</legend>
<form action="{URLSALVAFOTOATRIBUTO}" method="post">
	<input type="hidden" name="codproduto" value="{CODPRODUTO}">

	<table class="table table-bordered table-condensed">
		<tr>
			<th style="width: 80px;">Imagem</th>
			<th>Atributos</th>
		</tr>
		{BLC_FOTOS}
		<tr>
			<td><img src="{URLIMAGEM}"></td>
			<td><select name="skus[{CODPRODUTOFOTO}][]" multiple="multiple">
					{BLC_SKUSPRODUTO}
					<option value="{CODSKU}" {SEL_SKU}>{NOMESKU}</option> {/BLC_SKUSPRODUTO}
			</select>
		
		</tr>
		{/BLC_FOTOS}
		{BLC_SEMFOTOS}
		<tr>
			<td colspan="2">Não foram encontradas fotos para este produto.</td>
		</tr>
		{/BLC_SEMFOTOS}

	</table>
<br>
<button type="submit" class="btn btn-primary">Salvar</button>	<?=anchor('painel/produto','Voltar',['class'=>'btn btn-danger'])?>
	
</form>

                                                                             