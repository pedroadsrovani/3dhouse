
<legend> 
Manutenção de Usuários - {ACAO}<!--novo saber que esta adicionando novo admin -->

</legend> 

  
<form action="{ACAOFORM}" method="post" class="form-horizontal">  <!--aaçã do form sera definida via controler/ method post(evia dados) class classe do boosttrap-->
<!-- formulario nome -->  
<!-- o atributo html name lista no campo os dados recuperados para facilitar a edição desses dados --> 
<!-- o atributo value mostra o valor daquele campo  --> 
<!--  propriedade required define que este camnpo é obrigatorio preencher de dados--> 
      <input type="hidden" name="codusuario" id="codusuario" value="{codusuario}"><!-- campo de chave primaria do formulario para edição -->
<label>Nome do usuário<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  type="text" id="nome" class="form-control" name="nome" value="{nome}" placeholder="Digite um nome...">
     <label class="control-label" for="nome"></label>
  </div>

</br>
<label>E-mail do usuario<font color="#FF0000"> *</font></label>
 <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
    <input  type="email" id="email" class="form-control" name="email" value="{email}" placeholder="Digite o e-mail...">
     <label class="control-label" for="email"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Exemplo: example@gmail.com
    </small> 
</br>
</br>
<label>Senha do usuário <font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
    <input  type="password" id="senha" class="form-control" name="senha" >
     <label class="control-label" for="senha"></label> 
     
  </div> 
  <small id="passwordHelpInline" class="text-muted">
      A senha deve conter de 8 a 20 caracteres.
    </small>  
    </br>
    </br>
<label>Digite um cpf para o usuário<font color="#FF0000"> *</font></label>
   <div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    <input  type="text" id="cpf" class="form-control" name="cpf" value="{cpf}">
     <label class="control-label" for="cpf"></label>
  </div>  
  <small id="passwordHelpInline" class="text-muted">
      Sem traços. Ex:12345678911
    </small>  
  </br>
  </br>
<label>Digite um cep e depois clique em procurar <font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="cep" class="form-control" style="width:50%" name="cep" value="{cep}">
     <label class="control-label" for="cep"></label>  <button id="btn_consulta" style="margin-left: 10px;" class="btn btn-primary" type="button" >Consultar</button>
  </div>  

</br> 
<label>Cidade<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="cidade" class="form-control" style="width:50%" name="cidade" value="{cidade}"/>
     <label class="control-label" for="cidade"></label>
  </div>   
   
  </br>
<label>Estado<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="estado" class="form-control" style="width:50%" name="estado" value="{estado}"  />
     <label class="control-label" for="estado"></label>
  </div> 
<label>Rua<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="rua" class="form-control" name="rua" value="{rua}">
     <label class="control-label" for="rua"></label>
  </div>  
   
  </br>
<label>Bairro<font color="#FF0000"> *</font></label>
<div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="bairro" class="form-control" name="bairro" value="{bairro}">
     <label class="control-label" for="bairro"></label>
  </div>  
 
   
  </br> 

  <div class="form-group">
  <label class="control-label" for="complemento">Complemento (opcional)</label> 
    
       <textarea class="form-control" id="complemento" name="complemento" rows="6" placeholder="Digite aqui...">{complemento}</textarea>

     <label class="control-label" for="complemento"></label>
  </div>   

 

<label>Telefone<font color="#FF0000"> *</font></label>
  <div class="input-group">
    <span class="input-group-addon"></span>
    <input  type="text" id="telefone" class="form-control" name="telefone" style="width:50%" value="{telefone}">
     <label class="control-label" for="telefone"></label>
  </div>  
<small id="passwordHelpInline" class="text-muted">
      Sem traços. Ex: DDD123456789
    </small> 
   
  <!-- formulario ativo-->   
 </br>
</br> 
<!--chk_ativadoadmin variavel criada para marcar se o admin esta ativo ou inativo que sera usado na edição  -->  
<!--cada label deve conter essa variavel uma para ativo outra para desativado com ses valores s ou n   -->
<label>Sexo:</label> 
</br>
<label class="radio-inline"><input type="radio" name="sexo"  id="sexo" value="masculino" {chk_sexo}>Masculino</label>
<label class="radio-inline"><input type="radio" name="sexo" id="sexo" value="feminino" {chk_sexo}>Feminino</label>
  
  </br>       
  </br> 
  <!-- checkbox perfil -->   
  <label>Perfil:</label> 
  </br>
<label class="radio-inline"><input type="radio" name="perfil" id="perfil" value="administrador" {chk_perfil}>Administrador</label>
<label class="radio-inline"><input type="radio" name="perfil" id="perfil" value="vendedor" {chk_perfil}>Vendedor</label>
<label class="radio-inline"><input type="radio" name="perfil" id="perfil" value="cliente" {chk_perfil}>Cliente</label>
 </br> 
 </br>
  
  <button type="submit" class="btn btn-primary">Salvar</button><!-- botão do tipo submit para enviar dados -->  <a href="{URLLISTAR}" title="Listar Usuários" class="btn btn-primary">Voltar</a><!--link para voltar para a listagem-->

</form>  <!--Observação: o botão de salvar precisa estar dentro da tag form senão os dados não vão ser salvos -->

<!-- passo 12 definitr as variaveis ex ACAOFORM,URLLISTAR no controle de admins arquivo controler/painel/admin.php-->
 <!--passo 15 voltar e definir o metodo privado para acaoform e urllistar --> 