
<legend> 
Listagem  de Produtos<br>

<a href="{URLLISTAR}" title="Listar produtos" class="btn pull-right"><button type="button" class="btn btn-primary"><em class="glyphicon glyphicon-th-list"></em> Listar</button></a><!--joga os usuarios listados a direita --> 
<a href="{URLADICIONAR}" title="Adicionar produtos" class="btn pull-right"><button type="button" class="btn btn-primary">  <em class="glyphicon glyphicon-asterisk"></em> Novo</button></a><!--joga os usuarios listados a direita -->

</legend> 
<table class="table table-bordered table-responsive table-condensed"> 
   <tr>  
        
        <th class="coluna-acao text-center">Manutenção</th>  
        <th style="width: 50px;">Código</th>  
        <th>Nome</th>  
         <th style="width: 20px;">R$</th> 
        
        
   </tr> 

   {BLC_DADOS}  
   <tr>   

  

  <td><div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><em class="glyphicon glyphicon-pencil"></em> Editar 
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="{URLEDITAR}">Editar Produto</a></li>
      <li><a href="{URLATRIBUTOS}">Editar Atributos & Estoque</a></li>  
       <li><a href="{URLVINCULAIMAGEMSKU}">Vincular imagens com atributos</a></li> 

      <li><a href="{URLUPLOAD}">Adicionar Fotos</a></li> 
       <li><a href="{URLEXCLUIR}">Excluir Produto</a></li>
    </ul>
 
</div>
</td>
        <td>{CODPRODUTO}</td> 
        <td>{NOME}</td>  
         <td>{PRECO}</td> 
       

   </tr> 
   {/BLC_DADOS} 
   {BLC_SEMDADOS}  
   <tr> 
        
        <td colspan="4" class="text-center"> Não há dados!</td> 
        
   </tr> 
   {/BLC_SEMDADOS}
</table> 
<nav aria-label="..." class="text-center">
	<ul class="pagination pagination-sm">
		<li class="{HABANTERIOR}"><a href="{URLANTERIOR}">&laquo;</a>
		{BLC_PAGINAS}
		<li class="{LINK}"><a href="{URLLINK}">{INDICE}</a></li>
		{/BLC_PAGINAS}
		<li class="{HABPROX}"><a href="{URLPROXIMO}">&raquo;</a>
	</ul>
</nav> 

