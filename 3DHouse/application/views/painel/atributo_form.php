<legend>
Manutenção de Atributos - {ACAO}
	
</legend>
<form action="{ACAOFORM}" method="post" class="form-horizontal">
	<input type="hidden" name="codatributo" id="codatributo" value="{codatributo}">
	
	<div class="input-group">
    <span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
    <input type="text" id="nomeatributo" class="form-control" name="nomeatributo" value="{nomeatributo}" required="required" placeholder="Digite um nome de atributo..">
     <label class="control-label" for="nomeatributo"></label>
  </div>  
<br>
	

	<div class="control-group">
		<label class="control-label" for="codtipoatributo">Tipo:</label>
	    <div class="controls">
	    	<select class="form-control" name="codtipoatributo" id="codtipoatributo" >
	    		<option value="">Selecione o tipo do atributo</option>
	    		{BLC_TIPOATRIBUTOS}
	    		<option value="{CODTIPOATRIBUTO}" {sel_sel_codtipoatributo}>{NOME}</option><!-- {sel_sel_codtipoatributo} -->
	    		{/BLC_TIPOATRIBUTOS}
	    	</select>
	    </div>
	</div>
  	<br>
		<button type="submit" class="btn btn-primary">Salvar</button>  <a href="{URLLISTAR}" title="Listar atributos" class="btn btn-primary">Voltar</a><!--joga os usuarios listados a direita -->
	
</form>