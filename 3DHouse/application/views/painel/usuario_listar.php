<legend> 
Listagem de Nomes<br>
<!-- URLLISTAR lista os admins já adicionados --> 
<!-- URLADICIONAR bloco que chama o metodo adicionar admins -->
<a href="{URLLISTAR}" title="Listar usuarios" class="btn pull-right"><button type="button" class="btn btn-primary"><em class="glyphicon glyphicon-th-list"></em> Listar</button></a><!--joga os usuarios listados a direita --> 
<a href="{URLADICIONAR}" title="Adicionar usuarios" class="btn pull-right"><button type="button" class="btn btn-primary">  <em class="glyphicon glyphicon-asterisk"></em> Novo</button></a><!--joga os usuarios listados a direita -->

</legend> 
<table class="table table-bordered table-condensed"> <!-- classe bootastrap para tabelas -->
   <tr> 
        <th class="coluna-acao text-center">Manutenção</th>   
        <th>Nome</th>  
        
        <th class="coluna-acao text-center">Perfil</th>  

   </tr> 

   {BLC_DADOS} <!-- bloco para listagem de dados --> 
   <tr>  
      

      <td><div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><em class="glyphicon glyphicon-pencil"></em> Editar 
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="{URLEDITAR}">Editar usuario</a></li>
      <li><a href="{URLEXCLUIR}">Excluir usuario</a></li>
      
    </ul>
 
</div>
</td>
        
        <td>{NOME}</td> <!-- bloco que busca o nome do admin -->
        <td>{PERFIL}</td><!-- antigo {ATIVO}-->

   </tr> 
   {/BLC_DADOS} 
   {BLC_SEMDADOS}  <!-- bloco que mostra quando não há dados para serem mostrados -->
   <tr> 
        
        <td colspan="3" class="text-center"> Não há dados!</td> 
        
   </tr> 
   {/BLC_SEMDADOS}
</table>  
<!-- html de paginação --> 
<!--  
HABAANTERIOR link para navegação da aba anterior da paginação
URLANTERIORlink para voltar a pagina anterior
LINK 
URLLINK
URLPROXIMO 
HABPROX 
BLC_PAGINAS bloco de paginação 
INDICE
-->
<nav aria-label="..." class="text-center">
	<ul class="pagination pagination-sm">
		<li class="{HABANTERIOR}"><a href="{URLANTERIOR}">&laquo;</a>
		{BLC_PAGINAS}
		<li class="{LINK}"><a href="{URLLINK}">{INDICE}</a></li>
		{/BLC_PAGINAS}
		<li class="{HABPROX}"><a href="{URLPROXIMO}">&raquo;</a>
	</ul>
</nav>
<!-- criar um arquivo .css para o template administrativo  --> 
<!-- criar a função index no controler de admins  -->