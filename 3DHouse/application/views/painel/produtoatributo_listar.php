<legend>
Atributos do Produto: {NOMEPRODUTO}<br><!-- bloco de nome do produto-->
	
</legend>
<form action="{URLSALVAATRIBUTO}" method="post">
<input type="hidden" name="codproduto" value="{CODPRODUTO}"><!-- chave primaria do formulario-->
<h5>Atributos e Quantidade de Estoque Vinculados ao <b>{NOMEPRODUTO}</b></h5>

<table class="table table-bordered table-condensed">
	<tr>
		<th style="width:80px;">Código - Ref</th>
		<th>Descrição</th>
		<th style="width:50px;">Quantidade</th>
		<th class="coluna-acao text-center">Remover</th>
	</tr>
	{BLC_SEMVINCULADOS}<!-- não há nenhum atributo vinculado -->
	<tr>
		<td colspan="4">Não há atributos vinculados.</td>
	</tr>
	{/BLC_SEMVINCULADOS}
	{BLC_VINCULADOS}<!--exibe todos os skus vinculados com atributos -->
	<tr>                                                  <!-- indentificação referente ao campo referencia da tabela sku -->
		<td><input type="text" class="input form-control" name="sku[{CODSKU}][referencia]" value="{REFERENCIA}"></td>
		<td>{DESCRICAO}</td><!-- descrição do sku  ou referencia-->                        <!--mostra o que tiver no bloco REFERENCIA no controler --> 
		                                                                                   <!--mostra o que tiver no bloco QUANTIDADE no controler -->
		<td><input type="text" class="input form-control" name="sku[{CODSKU}][quantidade]" value="{QUANTIDADE}"></td> 
		           
				   
				                                           <!--indentificação referente ao campo quantidade da tabela sku -->
		<td><input type="checkbox" class="custom-control-input" name="sku[{CODSKU}][remover]" value="S"></td> <!--indentificação referente ao metodo de remover sku -->
	    
	</tr>
	{/BLC_VINCULADOS}
</table>

<h5>Atributos Disponíveis para o <b>{NOMEPRODUTO}</b></h5>

<table class="table table-bordered table-condensed">
	<tr>
		<th style="width:80px;">Código - Ref</th>
		<th>Descrição</th>
		<th style="width:50px;">Quantidade</th>
	</tr>
	{BLC_SEMDISPONIVEIS}<!--não há atributos disponíveis -->
	<tr>
		<td colspan="3">Não há atributos disponíveis para este produto.</td>
	</tr>
	{/BLC_SEMDISPONIVEIS}
	{BLC_DISPONIVEIS}<!--bloco que mostra os atributos disponíveis -->
	<tr>                                                  <!--mostra o atributo junto com a referencia do sku-->
		<td><input type="text" class="input form-control" name="atributo[{CODATRIBUTO}][referencia]" value=""></td>
		<td>{DESCRICAO}</td> <!-- bloco de descrição do atributo--> 
		                                                  <!--mostra o atributo junto com a quantidade do sku-->
		<td><input type="text" class="input form-control" name="atributo[{CODATRIBUTO}][quantidade]" value=""></td>
	</tr>
	{/BLC_DISPONIVEIS}
</table>
<br>
		<button type="submit" class="btn btn-primary">Salvar</button>  <a href="{URLLISTAR}" title="voltar" class="btn btn-primary">Voltar</a><!--joga os usuarios listados a direita -->
	
</form>