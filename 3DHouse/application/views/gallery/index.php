<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">
<head>
   <meta charset="utf-8">
   <title>CodeIgniter Image Gallery</title>

   <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
   <style type="text/css">

   ::selection { background-color: #E13300; color: white; }
   ::-moz-selection { background-color: #E13300; color: white; }

  

   a {
      color: #003399;
      background-color: transparent;
      font-weight: normal;
   }

   

   code {
      font-family: Consolas, Monaco, Courier New, Courier, monospace;
      font-size: 12px;
      background-color: #f9f9f9;
      border: 1px solid #D0D0D0;
      color: #002166;
      display: block;
      margin: 14px 0 14px 0;
      padding: 12px 10px 12px 10px;
   }

   
   .caption{ 
       width: 200%;
   }
  p{ 
    width: 100%; 
    height: 100%; 
  }
   
   </style>
</head>
<body>

<div id="container"> 
    <div id="body">
      <?php if($imagens->num_rows() > 0) : ?>
         
         <?php if($this->session->flashdata('message')) : ?>
            <div class="alert alert-success" role="alert" align="center">
               <?=$this->session->flashdata('message')?>
            </div> 
        <?php endif; ?>    
   <h3>Galeria de Imagens do produto: <small id="passwordHelpInline"  class="text-muted">
       Clique no botão abaio para adicionar um banner
    </small>  </h3> 
    </br>

   
         

         <div align="center"><?=anchor('gallery/add','Adicionar imagem',['class'=>'btn btn-success'])?></i></div> 
         
         <hr />    
         <div class="row">
            <?php foreach($imagens->result() as $img) : ?>
            <div class="col-md-3">
               <div class="thumbnail">
                  <?=img($img->tipo)?>
                  <div class="caption">
                     <h3><?=substr($img->comentario, 0,5)?>...</h3>
                     <p><?=substr($img->descricao, 0,5)?>...</p>
                     <p> 
                         
                        <?=anchor('gallery/edit/'.$img->id,'Editar',['class'=>'btn btn-primary btn-xs', 'role'=>'button'])?> <?=anchor('gallery/delete/'.$img->id,'Excluir',['class'=>'btn btn-danger btn-xs', 'role'=>'button'])?>
                       
                     </p>
                  </div>
               </div>
            </div>
            <?php endforeach; ?>
         </div>
      <?php else : ?>
         <div align="center"> <?=anchor('gallery/add','Adicionar imagens',['class'=>'btn btn-success', 'role'=>'button'])?></div>
      <?php endif; ?>
   </div>

</div>

</body>
</html>