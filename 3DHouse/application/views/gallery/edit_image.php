<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->input->post()){
   $comentario       = set_value('comentario');
   $descricao    = set_value('descricao');
} else {
   $comentario       = $imagens->comentario;
   $descricao    = $imagens->descricao;
}
?><!DOCTYPE html>
<html lang="pt-br">
<head>
   <meta charset="utf-8">
   <title>Alterar imagem</title>

   <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
   
   <style type="text/css">

   ::selection { background-color: #E13300; color: white; }
   ::-moz-selection { background-color: #E13300; color: white; }

   

   a {
      color: #003399;
      background-color: transparent;
      font-weight: normal;
   }

  

   code {
      font-family: Consolas, Monaco, Courier New, Courier, monospace;
      font-size: 12px;
      background-color: #f9f9f9;
      border: 1px solid #D0D0D0;
      color: #002166;
      display: block;
      margin: 14px 0 14px 0;
      padding: 12px 10px 12px 10px;
   }

   #body {
      margin: 0 15px 0 15px;
   }

   
   </style>
</head>
<body>


   <h3>Alterar imagem</h3>

   <div id="body">
      <?php if(validation_errors() || isset($error)) : ?>
         <div class="alert alert-danger" role="alert" align="center">
            <?=validation_errors()?>
            <?=(isset($error)?$error:'')?>
         </div>
      <?php endif; ?>
      <?=form_open_multipart('gallery/edit/'.$imagens->id)?>

        <div class="form-group">
          <label for="userfile"></label> 
          
          <div class="row" style="margin-bottom:5px"><div class="col-xs-12 col-sm-6 col-md-3"><?=img(['src'=>$imagens->tipo,'width'=>'100%'])?></div></div>
          
          <input type="file" class="form-control" style="whidth:50%" name="userfile">
        </div>

        <div class="form-group">
          <label for="comentario">Comentário</label>
          <input type="text" class="form-control" name="comentario"  value="<?=$comentario?>">
        </div>

        <div class="form-group">
          <label for="descricao">Descrição</label>
          <textarea class="form-control" rows="6" name="descricao"><?=$descricao?></textarea>
        </div>
        </br>
        <button type="submit" class="btn btn-primary">Salvar</button>
        <?=anchor('gallery','Cancelar',['class'=>'btn btn-danger'])?>

      </form>
   </div>



</body>
</html>