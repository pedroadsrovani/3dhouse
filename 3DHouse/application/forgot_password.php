 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
 <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
<style> 

.form-gap{ 
  margin-top: 50px;
}

</style>

 <div class="form-gap"></div>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <h2 class="text-center">Esqueceu a senha?</h2>
                  <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
                  <div class="panel-body">
    <?php if(isset($message)){  ?>
 

 
 <?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?= $this->session->flashdata('message') ?>
                        </div>
                    <?php endif; ?>
                    <?=form_open('auth/forgot_password')?>
                      <div class="form-group">
                        <div class="input-group"> 
						<!--<label for="identity"><?php #echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />-->
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span> 
                          <input id="identity" name="identity" placeholder="digite seu e-mail..." class="form-control"  type="email">
                        </div>
                      </div>
                      <div class="form-group">
                        <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Redefinir Senha" type="submit">
                      </div>
                      
                      <input type="hidden" class="hide" name="token" id="token" value=""> 
                    <?php echo form_close();?>  
    
                  </div>
                </div>
              </div>
            </div>
          </div>
	</div>
