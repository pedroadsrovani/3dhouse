<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends My_Controller {//nome da classe
   public function __construct(){ /*  função ou classe de construção para carregar o model do usuário  */
        parent::__construct();
        $this->layout = LAYOUT_PAINELADMIN;// define o layout do painel admin ou carrega
        $this->load->model('Usuario_Model', 'UsuarioM'); //carrega o model de admin
        $this->load->library('encrypt');//carrega a biblioteca de encriptação
        $this->load->library('curl');  
        //autenticaçao
        $this->load->library('ion_auth'); 




   }

  /*  função index declaração dos blocos de conteudo e paginação */
   public function index(){ 

    if (!$this->ion_auth->logged_in())
    {
      // redirect them to the login page
      redirect('auth/login', 'refresh');
    } 
    elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
    {
      // redirect them to the home page because they must be an administrator to view this
      return show_error('You must be an administrator to view this page.');
    } 

    else
    {
      // set the flash data error message if there is one
      //$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

      //list the users
     // $this->data['users'] = $this->ion_auth->users()->result();
      //foreach ($this->data['users'] as $k => $user)
      //{
        //$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
      //}

      

#definição dos blocos de conteudo e lincagem
       $data = array();
       $data['URLADICIONAR'] = site_url('painel/usuario/adicionar'); #chamada da função adicionar pelo metodo url
       $data['URLLISTAR'] = site_url('painel/usuario');   #chamada da função listar pelo metodo url utilizando função url da biblioteca helper
       $data['BLC_DADOS'] = array();  #dados do sistema
       $data['BLC_SEMDADOS'] = array(); #exibe mensagem que não tem dados no bd
       $data['BLC_PAGINAS'] = array();
       $pagina = $this->input->get('pagina'); #definição da variavel pagina

       if(!$pagina){ #se não estiver setada

         $pagina = 0;
       }else{

           $pagina = ($pagina - 1) * LINHAS_PESQUISA_PAINELADMIN;//para exibir os proximos 7 itens da tabela
       }

       $res = $this->UsuarioM->get(array(), FALSE, $pagina);#variável que irá receber dados contidos na tabela admin os dados vindos da tabela são adicionados em um array para realizar a função de paginação
       #parametro false indica se irá vir uma ou todas as linhas da pesquisa
       #variável $pagina váriavel utilizada para paginação
       if($res){ #verifica se vier dados
           foreach($res as $r){ /* se os dados vierem o for each irá percorrer cada linha e adicionará aa variaável $data */
                  $data['BLC_DADOS'][] = array(
                  "PERFIL" =>$r->perfil,
                  "NOME" => $r->nome, #dados atuais em que o for each estará percorrendo irá ser mostrado na tabela de listar
                  "URLEDITAR" => site_url('painel/usuario/editar/'.$r->codusuario),
                  "URLEXCLUIR" => site_url('painel/usuario/excluir/'.$r->codusuario)
               );//se vier dados sera adicionado ao array
           }

       }else{
           $data['BLC_SEMDADOS'][] = array();


       }

       #indice de paginação

       $totalItens = $this->UsuarioM->getTotal(); #pega o total de itens da tabela admin
       $totalPaginas = ceil($totalItens / LINHAS_PESQUISA_PAINELADMIN);  /* metodo para dividir as linhas retornadas   */
       $indicePg = 1;  #indice para criar um laço para paginação
       $pagina = ($pagina ==0)?1:$pagina;

           if($totalPaginas > $pagina){
               $data['HABPROX'] = null; #não habilita para a proxima pagina se o totoal de itens não for naior que LINHAS_PAINEL_ADMIN
               $data['URLPROXIMO'] = site_url('painel/usuario?pagina='.$pagina + 1);#aumenta um item para prosseguir paara a próxima página de intens
           }else{
               $data['HABPROX'] = 'disabled'; #desabilita ser o total de itens não ultrapassar o valor da variavel LINHAS_PAINEL...
               $data['URLPROXIMO'] = '#';
           }

           if($pagina<=1){ #não poderá voltar para a pagina anterior dsabilitando o link
               $data['HABANTERIOR'] = 'disabled';
               $data['URLANTERIOR'] = '#';
           }else{
                $data['HABANTERIOR'] = null;
               $data['URLANTERIOR'] = site_url('painel/usuario?pagina='.$pagina - 1); # o usuário poderá regressar para a pagina anterior

           }


         while($indicePg <= $totalPaginas){ #navegará pelo total de paginas adicionando um botão para o total de itens correspondentes
             $data['BLC_PAGINAS'][] = array(
                 "LINK" => ($indicePg==$pagina)?'active':null,
                 "INDICE" => $indicePg,
                 "URLLINK" =>  site_url('painel/usuario?pagina='.$indicePg)

             );
             $indicePg++;
         }


       $this->parser->parse('painel/usuario_listar', $data); #chamada do formulário de usuário listar
   } 

}
 function login()
    {
        $this->render(painel/principal);
    }
 
    public function logout()
    {
        redirect("auth/login", 'refresh');
    }


/*  fim do index */
/* função de adicionar  */
# diferenciar variaveis que vem do banco de dados como minusculas e que não são do banco deixar MAIUSCULAS
   public function adicionar(){ //definir as variaveis do formulario de admin_form
       $data = array();  //joga os dados do formulario em um array
       $data['ACAO'] = 'Novo'; //definir novo para saber que esta adicionando um novo admin
       $data['codusuario'] = ''; //definir o codigo do usuario como vazio
       $data['nome'] = '';
        $data['email'] = ''; //definir o nome do admin vazio
       $data['cpf'] = ''; //definir o email do admin vazio
       $data['cep'] = ''; //definir o codigo do usuario como vazio
       $data['rua'] = ''; //definir o nome do admin vazio
       $data['bairro'] = ''; //definir o email do admin vazio
       $data['complemento'] = ''; //definir o codigo do usuario como vazio
       $data['cidade'] = ''; //definir o nome do admin vazio
       $data['estado'] = ''; //definir o email do admin vazio
       $data['telefone'] = ''; //definir o codigo do usuario como vazio

       $data['chk_sexo'] = '';//definir o campo ativado do admin vazio

       $data['chk_perfil'] = '';
       $this->setURL($data); #chamada da função privada setURL utilizando esse metodo evita a duplicação destes itens ACAOFORM, URLLISTAR etc..

       $this->parser->parse('painel/usuario_form', $data);//$data são os valores das variaveis do form


   }
/* fim do adicionar*/
#função editar

public function editar($id) {#função editar recebe como parametro o codigo do admin
		$data						= array();#$data  recebe um array
		$data['ACAO']				= 'Edição';# bloco ACAO recebe mensagem 'novo'

        #retorna apenas uma linha passando true como parametro através da expressão get no model admin_model
		$res	= $this->UsuarioM->get(array("codusuario" => $id), TRUE);# rerorna através do model o admin
		#condição do array será o codadmin equivale ao $id passado
		if ($res) {#se vier dados
        # $valor será a variável que será preenchida
        #$chave corresponde ao item de chave primaria do formulário ou (<input type "hidden" value ={codusuário}> )com valores do respectivo codigo do administrador
			foreach($res as $chave => $valor) {#percorre o formulario preenchendo os campos do formulario de editar com os dados do respectivo admin  passado por parametro
				$data[$chave] = $valor;
			}

        $data['chk_sexo']	= 'disabled="disabled"';#desabilita o campo tipo atributo na edição





		} else {# se não vier dados
			show_error('Não foram encontrados dados.', 500, 'Ops, erro encontrado.');
		}

		$this->setURL($data);

		$this->parser->parse('painel/usuario_form', $data);#chamada do formulário com os dados contidos do respectivo admin salvos na variável $data
	}
 /*fim do aditar */
   #função salvar

   public function salvar(){
  // observação: o nome das variáveis tem que ser da mesma forma que tiver salvo no bd senão irá ocorrer erro 1054 error database
 // usa-se input quando se tem uma entrada de dados nos campos abaixo utilizado no formulário
       $codusuario = $this->input->post('codusuario'); //codadminque foi recuperado através do método post(pegar)
       $nome = $this->input->post('nome'); //nomeadmin foi recuperado através do método post(pegar)
       $email = $this->input->post('email'); //emailadmin que foi recuperado através do método post(pegar)
       $senha = $this->input->post('senha'); //senhadmin que foi recuperado através do método post(pegar)
       $cpf = $this->input->post('cpf');
       $cep = $this->input->post('cep');
       $rua = $this->input->post('rua');
       $bairro = $this->input->post('bairro');
       $complemento = $this->input->post('complemento');
       $cidade = $this->input->post('cidade');
       $estado = $this->input->post('estado');
       $telefone = $this->input->post('telefone');
       $sexo = $this->input->post('sexo'); //ativadoadmin que foi recuperado através do método post(pegar)
       $perfil = $this->input->post('perfil');

       #validar os dados digitados no formulario verificação de código.
       $erro = FALSE; //tipo do erro do tipo booleano
       $mensagem = null;  //mensagem que sera mostrada

// esse tipo de verificação é uma verificação booleana
           // se não existir os dados no post equivale a false.
           if(!$nome){ //verificação se a variavel
               $erro  = TRUE;
               $mensagem .= "Informe o nome de usuário\n"; // .= operador do php para concatenar mensagens
           }


// esse tipo de verificação é uma verificação booleana
           // se não existir os dados no post equivale a false.
           if(!$email){
               $erro  = TRUE;
               $mensagem .= "Informe o seu E-mail\n";
           }



           // esse tipo de verificação é uma verificação booleana
           // se não existir os dados no post equivale a false.
           if(!$senha){ // verificá se um admin for novo

                if(!$codusuario){ #exige a senha do admin
                   $mensagem .= "Informe a sua senha de acesso\n"; //mensagem de erro
             }
         }



            if(!$erro){  //se não houve erros
              $itens = array( // criação do objeto itens
                "nome" => $nome,
                "email" => $email,
                "cpf" => $cpf,
                "cep" => $cep,
                "rua" => $rua,
                "bairro" => $bairro,
                "complemento" => $complemento,
                "cidade" => $cidade,
                "estado" => $estado,
                "telefone" => $telefone,
                "sexo" => $sexo,
                "perfil" => $perfil //tratamento de dados com condição do php abreviada (se existe ativado ou desativado ele vai equivaler o valor que veio do bd, se não existe equivale a N  )
                // são os campos que terão seus dados validados através do código então se o admin esqueceu de preenche-los, então irá apatrecer uma msg de erro
              );

             if($senha){ //se tiver admin e ele vier

                 $itens['senha'] = $this->encrypt->encode($senha);// adiciona senha do admin encriptada ao encode
             }


               if($codusuario){  //se o codigo do usuario veio preenchido
                  $codusuario =  $this->UsuarioM->update($itens, $codusuario); // update //itens = objeto, codadmin = codadmin do bd // codadmin= this.--necessário para verificar se os dados vieram

               } else{ //se não está preenchido

                 $codusuario = $this->UsuarioM->post($itens);
               }


               if($codusuario){//se os dados vierem utiliza a sessão flash data para sucesso
               //observação:// redirect se não colocar o caminho de redirecionamento correto, irá acusar um erro 404 nonavegador(conferir o caminho correto)
                   $this->session->set_flashdata('sucesso', 'Dados inseridos com sucesso!');
                   redirect('painel/usuario');//redireciona para não ocorrer um duplo salvamento mandando o admin para a listagem de admins


               } else{ //sessão em flash data mostrando erro caso algum erro acontecer durante o processo de salvar

                   $this->session->set_flashdata('erro', 'Ocorreu um erro ao realizar a operação.');

                   if($codusuario){ //  se for uma operração de adicionar
                       redirect('painel/usuario/editar/'.$codusuario); //redireciona para o painel de editar concatenando com o codigo do admin

                   } else{ //senão redireciona para o painel de adicionar admin
                   redirect('painel/usuario/adicionar');
                   }

               }



               }     else{

                   $this->session->set_flashdata('erro', ($mensagem));
                    if($codusuario){
                       redirect('painel/usuario/editar/'.$codusuario);

                   } else{
                   redirect('painel/usuario/adicionar');
                   }


               }

         }


          #função privada para definir acao e urllistar
          #lembre-se de remover as chaves
          private function setURL(&$data){ //data é a  variavel que sera retornada
          $data['URLLISTAR'] = site_url('painel/usuario');
          $data['ACAOFORM'] = site_url('painel/usuario/salvar');
   }

   #excluir administradores

        public function excluir($id){  #função excluir que recebe o id do admin com parametro
           $res = $this->UsuarioM->delete($id); #chamada do metodo excluir no model de admin
           if($res){ # se o método retornar
               $this->session->set_flashdata('sucesso', 'Usuário removido!');
         }else{ #se for false é um erro
           $this->session->set_flashdata('erro', 'Usuário não foi removido! Tente novamente');

         }
            redirect('painel/usuario');#redireciona para voltar ao formulario de listagem de admins
      }
      //faz a consulta do cep no viacep
      public function consulta(){

        $cep = $this->input->post('cep');

        $this->layout=null;
        $this->output->set_content_type('application/json')
        ->set_output( $this->curl->consulta($cep));

    }
 
}



#passo 11 criar um diretorio e um arquivo para a view pro painel view/painel/admin_form.php
#passo 13 definir uma constante que armazena o carregamento do layout no arquivo config/constants.php
#passo 14 definir  a primaria do formulario no diretorio view/painel/ admin_form
#passo 15 criar o metodo salvar atribuindo os respectivos valores as variaveis e criar a validação dos dos dados inseridos nos campos do formulario
#passo 16 criar o model para inserir os admins no diretorio model criando um arquivo php admin_model.php

#passo 17 criar uma view para listagem de usuários view/painel/admin_listar.php
#ir no model e admin e criar um método get para listar admins cadastrados
# ir no model de admin e criar um metodo para exclusão dew admins
#ir ao model criar o metodo de edição de admins