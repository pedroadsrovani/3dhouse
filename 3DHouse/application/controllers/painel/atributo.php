<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atributo extends  Auth_Controller {
	
	/* função construct e carregamento dos models e layout */
   public function __construct() {
		parent::__construct();
		$this->layout	= LAYOUT_PAINELADMIN;
		$this->load->model('Atributo_Model', 'AtributoM'); 
		$this->load->model('TipoAtributo_Model', 'TipoAtributoM');#carregamento do model tipo atributo necessário quando precisa pegar informações de outras tabelas
	}   

   /*  função set url base url do site */
   private function setURL(&$data) {
		$data['URLLISTAR']	= site_url('painel/atributo');
		$data['ACAOFORM']	= site_url('painel/atributo/salvar');
	}


   /* função index onde a variavel data e declarada de acordo com o conteudo da pagina */
	public function index(){ /* função index e paginação */
		$data					= array();
		$data['URLADICIONAR']	= site_url('painel/atributo/adicionar');
		$data['URLLISTAR']		= site_url('painel/atributo');
		$data['BLC_DADOS']		= array();
		$data['BLC_SEMDADOS']	= array();
		$data['BLC_PAGINAS']	= array();
		/* variavel pagina recebe o get do model  */
		$pagina			= $this->input->get('pagina');
		
		if (!$pagina) {
			$pagina = 0;
		  } else {
			      $pagina = ($pagina-1) * LINHAS_PESQUISA_PAINELADMIN;/* resultado do retorno das pesquisa do get */
		      }
		
		          $res	= $this->AtributoM->get(array(), FALSE, $pagina); /* res recebe o que tiver na tabela atributo*/

		if ($res) {
			foreach($res as $r) { /* foreach incrementa o que tiver em res nos campos do bloco de conteudo */
				$data['BLC_DADOS'][] = array(
					"NOME"		=> $r->nomeatributo,
					"NOMETIPO"	=> (empty($r->nomepai))?'-':$r->nomepai,
					"URLEDITAR"	=> site_url('painel/atributo/editar/'.$r->codatributo),
					"URLEXCLUIR"=> site_url('painel/atributo/excluir/'.$r->codatributo)
				);
			}
		} else {
			$data['BLC_SEMDADOS'][] = array();
		}
		   
		       /* paginação do sistema */
		$totalItens		= $this->AtributoM->getTotal();
		$totalPaginas	= ceil($totalItens/LINHAS_PESQUISA_PAINELADMIN); /* ceil atributo do php para divisão de itens */		
		$indicePg		= 1;
		$pagina			= ($pagina==0)?1:$pagina;
		
		if ($totalPaginas > $pagina) {
			$data['HABPROX']	= null;
			$data['URLPROXIMO']	= site_url('painel/atributo?pagina='.$pagina+1);
		} else {
			$data['HABPROX']	= 'disabled';
			$data['URLPROXIMO']	= '#';
		}
		
		if ($pagina <= 1) {
			$data['HABANTERIOR']= 'disabled';
			$data['URLANTERIOR']= '#';
		} else {
			$data['HABANTERIOR']= null;
			$data['URLANTERIOR']= site_url('painel/atributo?pagina='.$pagina-1);
		}
		
		
		
		while ($indicePg <= $totalPaginas) {
			$data['BLC_PAGINAS'][] = array(
				"LINK"		=> ($indicePg==$pagina)?'active':null,
				"INDICE"	=> $indicePg,
				"URLLINK"	=> site_url('painel/atributo?pagina='.$indicePg)
			);
			
			$indicePg++;
		}
		
		$this->parser->parse('painel/atributo_listar', $data);
	}
        
		/* fim da paginação e função index */ 

         /*  função adicionar */
  public function adicionar(){	
		$data							= array();
		$data['ACAO']					= 'Novo';
		$data['BLC_TIPOATRIBUTOS']		= array();
		$data['codatributo']		= '';
		$data['nomeatributo']		= '';
		
		
		$tipo	= $this->TipoAtributoM->get(array(), FALSE, 0, FALSE);# carrega os tipos de atributos disponíveis
		# parametros FALSE = não exibe só a primeira linha, o 0 inicial, sem limites
		foreach($tipo as $d){ /*  foreach dos dados da tabela atributo na função adicionar */
			$data['BLC_TIPOATRIBUTOS'][] = array(#bloco para tipos de atributo
				"CODTIPOATRIBUTO"		=> $d->codtipoatributo,
				"NOME"					=> $d->nometipoatributo,
				"sel_codtipoatributo"=> null
			);
		}
		
		$this->setURL($data);
		
		$this->parser->parse('painel/atributo_form', $data);
	}
    /* fim da função adicionar */
    /*  função salvar                       */
   public function salvar() {
		
		$codatributo		= $this->input->post('codatributo');
		$nomeatributo	= $this->input->post('nomeatributo');
		$codtipoatributo	= $this->input->post('codtipoatributo');
		
		$erros			= FALSE;
		$mensagem		= null;
		/* condições que verificam se os campos estão vazios  */
		if (!$nomeatributo) {
			$erros		= TRUE;
			$mensagem	.= "Informe nome do atributo.\n";
		} 
       if (!$codtipoatributo) {
			$erros		= TRUE;
			$mensagem	.= "Informe o tipo do atributo.\n";
		}

		if (!$erros) {
			$itens	= array(
				"nomeatributo"	=> $nomeatributo,
				"codtipoatributo"=> $codtipoatributo
			);
			
			/* verificação se os dados foram inseridos corretamente  */
		if ($codatributo) {
				$codatributo = $this->AtributoM->update($itens, $codatributo);#se codatributo for setado então é um update
			} else {
				$codatributo = $this->AtributoM->post($itens);# senão é um insert
			}
			
		if ($codatributo) {
				$this->session->set_flashdata('sucesso', 'Dados inseridos com sucesso.');
				redirect('painel/atributo');
			} else {
				$this->session->set_flashdata('erro', 'Ocorreu um erro ao realizar a operação.');
				
		if ($codatributo) {
					redirect('painel/atributo/editar/'.$codatributo);#se codatributo for setado então redireciona para editar
				} else {
					redirect('painel/atributo/adicionar');# senão é um insert pelo metodo post
				}
			}
		            } else {
			           $this->session->set_flashdata('erro', nl2br($mensagem));
		if ($codatributo) {
				redirect('painel/atributo/editar/'.$codatributo);
			} else {
				redirect('painel/atributo/adicionar');
			}
		}
		
	}
/*  fim da função salvar  */ 
/* função editar */
      public function editar($id) {
		$data							= array();
		$data['ACAO']					= 'Edição';
		$data['BLC_TIPOATRIBUTOS']		= array();
		#informações do atributo
		$res	= $this->AtributoM->get(array("a.codatributo" => $id), TRUE);  

        if ($res) { /* foreach que insere os dados alterados */
			    foreach($res as $chave => $valor) {
				        $data[$chave] = $valor;
			  }
			
		    } else {
			      show_error('Não foram encontrados dados.', 500, 'Ops, erro encontrado.');
		    }


		#informações do tipos do atributo
		$tipo	= $this->TipoAtributoM->get(array(), FALSE, 0, FALSE);#carrega todos os itens de tipo atributo disponíveis 
		
		foreach($tipo as $d){ /* foreach dos tipo dos atributos*/
			    $data['BLC_TIPOATRIBUTOS'][] = array(
				"CODTIPOATRIBUTO"		=> $d->codtipoatributo,
				"NOME"					=> $d->nometipoatributo,
				"sel_codtipoatributo"=> ($res->codtipoatributo ==$d->codtipoatributo)?'selected="selected"':null # verifica se o tipo atributo está nulo senão seleciona o tipo de atributo atribuido durante o salvar
			);
		}
	
		
		$this->setURL($data);
		
		$this->parser->parse('painel/atributo_form', $data);
	} 
    /* fim da função editar */ 
	/* função excluir */

    public function excluir($id) {
		$res = $this->AtributoM->delete($id);
		
		if ($res) {
			$this->session->set_flashdata('sucesso', 'Atributo removido com sucesso.');
		} else {
			$this->session->set_flashdata('erro', 'Atributo não pode ser removido.');
		}
		
		redirect('painel/atributo');
	   }

}  /* fim da classe atributo */